package com.example.ridwan_x230.tugas_teori_uas_parkir;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {
    Button btnMasihParkir;
    Button btnKeluarParkir;
    Button btnMotorMasuk;
    Button btnMotorKeluar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // inisialisasi button/tombol
        btnMasihParkir = (Button) findViewById(R.id.btnMasihParkir);
        btnKeluarParkir = (Button) findViewById(R.id.btnKeluarParkir);
        btnMotorMasuk = (Button) findViewById(R.id.btnMotorMasuk);
        btnMotorKeluar = (Button) findViewById(R.id.btnMotorKeluar);

        btnMasihParkir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Tampilkan semua anggota activity lewat intent
                Intent i = new Intent(getApplicationContext(),SemuaAnggotaActivity.class);
                startActivity(i);
            }
        });

        btnKeluarParkir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Tampilkan semua anggota activity lewat intent
                Intent i = new Intent(getApplicationContext(),SemuaKeluarActivity.class);
                startActivity(i);
            }
        });

        btnMotorMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Tampilkan tambah anggota activity lewat intent
                Intent i = new Intent(getApplicationContext(),TambahAnggotaActivity.class);
                startActivity(i);
            }
        });

        btnMotorKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Tampilkan tambah anggota activity lewat intent
                Intent i = new Intent(getApplicationContext(),KeluarActivity.class);
                startActivity(i);
            }
        });

    }
}
