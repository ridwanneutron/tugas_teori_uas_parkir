package com.example.ridwan_x230.tugas_teori_uas_parkir;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class KeluarActivity extends Activity implements View.OnClickListener{
    // Progress Dialog
    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();
    EditText inputNo;
    EditText inputNama;
    EditText inputJenis;
    EditText inputWaktuMasuk;
    EditText inputWaktuKeluar;
    Button buttonScan;

    // inisialisasi url tambahanggota.php
    private static String url_tambah_anggota = "http://192.168.43.116/crudjson/keluar.php";

    // inisialisasi nama node dari json yang dihasilkan oleh php (utk class ini
    // hanya node "sukses")
    private static final String TAG_SUKSES = "sukses";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keluar);

        // inisialisasi Edit Text
        inputNo = (EditText) findViewById(R.id.inputNo);
        inputNama = (EditText) findViewById(R.id.inputNama);
        inputJenis = (EditText) findViewById(R.id.inputJenis);
        inputWaktuMasuk = (EditText) findViewById(R.id.inputWaktuMasuk);
        inputWaktuKeluar = (EditText) findViewById(R.id.inputWaktuKeluar);


        // inisialisasi button
        buttonScan = (Button) findViewById(R.id.buttonScan);


        buttonScan.setOnClickListener(this);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null){
            if (result.getContents() == null){
                Toast.makeText(this, "Hasil tidak ditemukan", Toast.LENGTH_SHORT).show();
            }else{
                // jika qrcode berisi data
                try{
                    // converting the data json
                    JSONObject object = new JSONObject(result.getContents());
                    // atur nilai ke textviews
                    inputNo.setText(object.getString("no"));
                    inputNama.setText(object.getString("nama"));
                    inputJenis.setText(object.getString("jenis"));
                    inputWaktuMasuk.setText("0");
                    Date d = new Date();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    inputWaktuKeluar.setText(dateFormat.format(d).toString());
                    //inputAlamat.setText(object.getString("alamat"));
                    new BuatAnggotaBaru().execute();
                }catch (JSONException e){
                    e.printStackTrace();
                    // jika format encoded tidak sesuai maka hasil
                    // ditampilkan ke toast
                    Toast.makeText(this, result.getContents(), Toast.LENGTH_SHORT).show();
                }
            }
        }else{
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void onClick(View v) {
        // inisialisasi IntentIntegrator(scanQR)
        IntentIntegrator intentIntegrator = new IntentIntegrator(this);
        //intentIntegrator.setBeepEnabled(false);
        intentIntegrator.initiateScan();
    }

    /**
     * Background Async Task untuk menambah data anggota baru *
     */
    class BuatAnggotaBaru extends AsyncTask<String, String, String> {
        // sebelum memulai background thread tampilkan Progress Dialog
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(KeluarActivity.this);
            pDialog.setMessage("Menambah data..silahkan tunggu");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        //menambah data
        protected String doInBackground(String... args) {
            String no = inputNo.getText().toString();
            String nama = inputNama.getText().toString();
            String jenis = inputJenis.getText().toString();
            String waktumasuk = inputWaktuMasuk.getText().toString();
            String waktukeluar = inputWaktuKeluar.getText().toString();
            //Double usia = Double.parseDouble(inputUsia.getText().toString());

            // Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("no", no));
            params.add(new BasicNameValuePair("nama", nama));
            params.add(new BasicNameValuePair("jenis", jenis));
            params.add(new BasicNameValuePair("waktumasuk", waktumasuk));
            params.add(new BasicNameValuePair("waktukeluar", waktukeluar));

            // mengambil JSON Object dengan method POST
            JSONObject json = jsonParser.makeHttpRequest(url_tambah_anggota, "POST", params);

            // periksa respon log cat
            Log.d("Respon tambah anggota", json.toString());

            try {
                int sukses = json.getInt(TAG_SUKSES);
                if (sukses == 1) {
                    // jika sukses menambah data baru
                    Intent i = new Intent(getApplicationContext(), SemuaKeluarActivity.class);
                    startActivity(i);

                    // tutup activity ini
                    finish();
                }else {
                    // jika gagal dalam menambah data
                }
            } catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(String file_url){
            // hilangkan dialog ketika selesai menambah data baru
            pDialog.dismiss();
        }
    }

}
